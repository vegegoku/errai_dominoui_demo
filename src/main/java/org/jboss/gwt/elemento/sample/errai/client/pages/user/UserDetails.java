package org.jboss.gwt.elemento.sample.errai.client.pages.user;

import elemental2.dom.HTMLElement;
import elemental2.dom.Node;
import elemental2.dom.Text;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.datatable.CellRenderer;
import org.dominokit.domino.ui.datatable.TableRow;
import org.dominokit.domino.ui.forms.SwitchButton;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.header.BlockHeader;
import org.dominokit.domino.ui.media.MediaObject;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.Style;
import org.dominokit.domino.ui.style.Styles;
import org.jboss.gwt.elemento.core.IsElement;

import javax.inject.Inject;

import static org.jboss.gwt.elemento.core.Elements.img;

public class UserDetails implements IsElement<HTMLElement> {

    private Row rowElement = Row.create()
            .style().add(Styles.margin_0).get();
    private CellRenderer.CellInfo<User> cell;

    public UserDetails(CellRenderer.CellInfo<User> cell) {
        this.cell = cell;
        initDetails();
    }


    private void initDetails() {

        rowElement
                .addColumn(Column.span4()
                                .appendChild(new Text(String.valueOf(cell.getRecord().getId())))
                                .appendChild(new Text(cell.getRecord().getUsername()))
                                .appendChild(new Text(cell.getRecord().getName()))
                );

    }

    @Override
    public HTMLElement asElement() {
        return rowElement.asElement();
    }

}
