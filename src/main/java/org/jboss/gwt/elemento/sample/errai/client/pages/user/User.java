package org.jboss.gwt.elemento.sample.errai.client.pages.user;

import org.jboss.errai.common.client.api.annotations.Portable;
import org.jboss.errai.databinding.client.api.Bindable;

import java.util.Date;

@Portable
@Bindable
public class User {

    private Long id;
    private String name;
    private String username;
    private String email;
    private String phone;
    private String website;
    private Date creationDate;

    public User() {}

    public User(Long id, String name, String userName, Date creationDate) {
        this.id = id;
        this.name = name;
        this.username = userName;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String toString() {
        return "User: " + id + " " + name + " " + username + " " + creationDate;
    }


}
