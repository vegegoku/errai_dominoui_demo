package org.jboss.gwt.elemento.sample.errai.client.pages;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.infoboxes.InfoBox;
import org.dominokit.domino.ui.style.Color;
import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;
import org.jboss.gwt.elemento.core.IsElement;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Templated(value = "template.html")
@Page(role = DefaultPage.class)
public class MainPage implements IsElement {
    @Inject
    @DataField
    HTMLDivElement content;

    @PostConstruct
    private void preparePage() {
        DomGlobal.console.info("prepare page ------------ >");
        content.appendChild(Row.create()
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.email(), "MESSAGES", "15")
                        .setIconBackground(Color.WHITE)
                        .setIconColor(Color.PINK)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.ZOOM)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.devices(), "CPU USAGE", "92%")
                        .setIconBackground(Color.WHITE)
                        .setIconColor(Color.BLUE)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.ZOOM)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.alarm(), "ALARM", "07:00 AM")
                        .setIconBackground(Color.WHITE)
                        .setIconColor(Color.AMBER)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.ZOOM)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.gps_fixed(), "LOCATION", "Jordan")
                        .setIconBackground(Color.WHITE)
                        .setIconColor(Color.DEEP_PURPLE)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.ZOOM)))
                .asElement());

        content.appendChild(Row.create()
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.equalizer(), "BOUNCE RATE", "62%")
                        .setIconBackground(Color.TEAL)
                        .setBackground(Color.TEAL)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.EXPAND)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.flight_takeoff(), "FLIGHT", "02:59 PM")
                        .setIconBackground(Color.GREEN)
                        .setBackground(Color.GREEN)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.EXPAND)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.battery_charging_full(), "BATTERY", "Charging")
                        .setIconBackground(Color.LIGHT_GREEN)
                        .setBackground(Color.LIGHT_GREEN)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.EXPAND)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.brightness_low(), "BRIGHTNESS RATE", "40%")
                        .setIconBackground(Color.LIME)
                        .setBackground(Color.LIME)
                        .flip()
                        .setHoverEffect(InfoBox.HoverEffect.EXPAND)))
                .asElement());
    }

    @Override
    public HTMLElement asElement() {
        return content;
    }
}
