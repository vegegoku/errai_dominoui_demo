package org.jboss.gwt.elemento.sample.errai.client.pages.UserFormPage;

import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.Window;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLTextAreaElement;
import org.dominokit.domino.ui.datepicker.DateBox;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.style.Styles;
import org.dominokit.domino.ui.utils.HasChangeHandlers;
import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.ui.shared.api.annotations.AutoBound;
import org.jboss.errai.ui.shared.api.annotations.Bound;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.gwt.elemento.core.IsElement;
import org.jboss.gwt.elemento.sample.errai.client.pages.user.User;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

public class UserEditor implements IsElement<HTMLElement>, TakesValue<User> {

    private Row rowElement = Row.create()
            .style().add(Styles.margin_0).get();

    @Inject
    @AutoBound
    private DataBinder<User> binder;

    @Inject @Bound public HTMLInputElement id;
    @Inject @Bound public TextBox username;
    @Inject @Bound(property = "username") public HTMLInputElement username2;
    @Inject @Bound public TextBox name;
    @Inject @Bound(converter = DateConverter.class) public HTMLInputElement creationDate;
    @Inject @Bound(property = "creationDate") public DateBox creationDate2;
    //@Inject @Bound(converter = DateConverter.class, property = "creationDate") public DateBox creationDate3;

    @Override
    public HTMLElement asElement() {
        return rowElement.asElement();
    }

    @Override
    public User getValue() {
        Window.alert(binder.getModel().toString());
        return binder.getModel();
    }

    @Override
    public void setValue(User model) {
        binder.setModel(model);
    }

    @PostConstruct
    public void postConstruct() {
        rowElement.appendChild(id);
        rowElement.appendChild(username);
        rowElement.appendChild(username2);
        rowElement.appendChild(name);
        rowElement.appendChild(creationDate);
        rowElement.appendChild(creationDate2);
        //rowElement.appendChild(creationDate3);
    }
}
