package org.jboss.gwt.elemento.sample.errai.client.pages.UserFormPage;

import org.jboss.errai.databinding.client.api.Converter;

import java.util.Date;

public class DateConverter implements Converter<Date, String> {

    @Override
    public Date toModelValue(final String widgetValue) {
        if (widgetValue == null || widgetValue.equals("")) {
            return null;
        }
        final Date jsDate = new Date(widgetValue);
        return new Date((long) jsDate.getTime());
    }

    @Override
    public String toWidgetValue(final Date modelValue) {
        if (modelValue == null) {
            return "";
        } else {
            final Date jsDate = new Date(((Long) modelValue.getTime()).longValue());
            return jsDate.toString().substring(0, 10);
        }
    }

    @Override
    public Class<Date> getModelType() {
        return Date.class;
    }

    @Override
    public Class<String> getComponentType() {
        return String.class;
    }

}