package org.jboss.gwt.elemento.sample.errai.client;

import com.google.gwt.user.client.ui.IsWidget;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.style.ColorScheme;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ioc.client.api.EntryPoint;
import org.jboss.errai.ui.nav.client.local.ContentDelegation;
import org.jboss.errai.ui.nav.client.local.NavigatingContainer;
import org.jboss.errai.ui.nav.client.local.Navigation;
import org.jboss.errai.ui.nav.client.local.api.DelegationControl;
import org.jboss.gwt.elemento.core.Widgets;
import org.jboss.gwt.elemento.sample.errai.client.pages.MainPage;
import org.jboss.gwt.elemento.sample.errai.client.pages.TabPage;
import org.jboss.gwt.elemento.sample.errai.client.pages.Test1Page;
import org.jboss.gwt.elemento.sample.errai.client.pages.UserFormPage.UserFormPage;
import org.jboss.gwt.elemento.sample.errai.client.pages.user.UserPage;
import org.jboss.gwt.elemento.sample.errai.client.shell.DominoNavigation;
import org.jboss.gwt.elemento.sample.errai.client.shell.FooterWidget;
import org.jboss.gwt.elemento.sample.errai.client.shell.MainMenu;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@EntryPoint
public class ApplicationEntryPoint {
    @Inject private Navigation navPanel;
    @Inject private MainMenu menu;
    @Inject private FooterWidget footer;

    @PostConstruct
    private void init() {
        buildLayout();
        buildMainMenu();
        configureContentNegotiation();
        configureREST();
    }

    private void buildLayout() {
        DominoNavigation.layout.getLeftPanel().appendChild(menu.init());
        DominoNavigation.layout.getFooter().appendChild(footer);
        DominoNavigation.layout.showFooter();
        DominoNavigation.layout.show(ColorScheme.LIGHT_BLUE);
    }

    private void buildMainMenu() {
        menu.addMenuItem(MainPage.class.getSimpleName(), Icons.ACTION_ICONS.android());
        //menu.addMenuItem(Test1Page.class.getSimpleName(), "account_box");
        menu.addMenuItem(TabPage.class.getSimpleName(), Icons.ACTION_ICONS.tab());
        menu.addMenuItem(UserPage.class.getSimpleName(), Icons.ACTION_ICONS.verified_user());
        menu.addMenuItem(UserFormPage.class.getSimpleName(), Icons.ACTION_ICONS.verified_user());
    }

    private void configureContentNegotiation() {
        navPanel.setContentDelegation(new ContentDelegation() {
            @Override
            public void showContent(Object page, NavigatingContainer defaultContainer, IsWidget widget, Object previousPage, DelegationControl control) {
                DominoNavigation.layout.getContentPanel().appendChild(Widgets.asElement(widget));
                control.proceed();
            }

            @Override
            public void hideContent(Object page, NavigatingContainer defaultContainer, IsWidget widget, Object nextPage, DelegationControl control) {
                DominoNavigation.layout.getContentPanel().clearElement();
                control.proceed();

            }
        });
    }

    private void configureREST() {
        RestClient.setJacksonMarshallingActive(true); 
        RestClient.setApplicationRoot("https://jsonplaceholder.typicode.com/");
    }
}