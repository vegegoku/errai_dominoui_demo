package org.jboss.gwt.elemento.sample.errai.client.pages.UserFormPage;

import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.Window;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLLabelElement;
import elemental2.dom.HTMLInputElement;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.labels.Label;
import org.dominokit.domino.ui.style.Styles;
import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.ui.shared.api.annotations.AutoBound;
import org.jboss.errai.ui.shared.api.annotations.Bound;
import org.jboss.gwt.elemento.core.IsElement;
import org.jboss.gwt.elemento.sample.errai.client.pages.user.User;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

public class UserView implements IsElement<HTMLElement>, TakesValue<User> {

    private Row rowElement = Row.create()
            .style().add(Styles.margin_0).get();

    @Inject
    @AutoBound
    private DataBinder<User> binder;

    @Inject @Bound public HTMLLabelElement id;
    @Inject @Bound public HTMLLabelElement username;
    @Inject @Bound public HTMLLabelElement name;
    @Inject @Bound(converter = DateConverter.class) public HTMLLabelElement creationDate;

    @Override
    public HTMLElement asElement() {
        return rowElement.asElement();
    }

    @Override
    public User getValue() {
        Window.alert(binder.getModel().toString());
        return binder.getModel();
    }

    @Override
    public void setValue(User model) {
        binder.setModel(model);
    }

    @PostConstruct
    public void postConstruct() {
        rowElement.appendChild(id);
        rowElement.appendChild(username);
        rowElement.appendChild(name);
        rowElement.appendChild(creationDate);
    }
}
