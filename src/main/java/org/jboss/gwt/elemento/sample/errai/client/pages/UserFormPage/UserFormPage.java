package org.jboss.gwt.elemento.sample.errai.client.pages.UserFormPage;

import com.google.gwt.user.client.Window;
import elemental2.dom.Event;
import elemental2.dom.EventListener;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.infoboxes.InfoBox;
import org.dominokit.domino.ui.modals.ModalDialog;
import org.dominokit.domino.ui.style.Color;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;
import org.jboss.gwt.elemento.core.IsElement;
import org.jboss.gwt.elemento.sample.errai.client.pages.user.User;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Date;

@Templated(value = "../template.html")
@Page(path = "admin/UserFormPage")
public class UserFormPage implements IsElement {

    @Inject UserEditor editor;
    @Inject UserView view;

    @Inject Button updateUserButton;
    @Inject Button getUserButton;

    @Inject @DataField HTMLDivElement content;

    @PostConstruct
    private void preparePage() {
        User user = new User(1L, "Name", "UserName", new Date());
        editor.setValue(user);
        updateUserButton.setColor(Color.GREEN);
        updateUserButton.setTextContent("Update User");
        getUserButton.setColor(Color.BLUE);
        getUserButton.setTextContent("Get user details");

        updateUserButton.addClickListener(new EventListener() {
            @Override
            public void handleEvent(Event evt) {
                Long currentId = editor.getValue().getId();
                editor.getValue().setId(++currentId);
            }
        });
        getUserButton.addClickListener(new EventListener() {
            @Override
            public void handleEvent(Event evt) {
                ModalDialog.create("Modal title").appendChild(Card.create("Card title", editor.getValue().toString())).open();
            }
        });

        content.appendChild(editor.asElement());
        content.appendChild(updateUserButton.asElement());
        content.appendChild(getUserButton.asElement());
        content.appendChild(view.asElement());
        view.setValue(editor.getValue());


    }

    @Override
    public HTMLElement asElement() {
        return content;
    }


}
