package org.jboss.gwt.elemento.sample.errai.client.pages.user;

import elemental2.dom.*;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.DataTable;
import org.dominokit.domino.ui.datatable.TableConfig;
import org.dominokit.domino.ui.datatable.plugins.RecordDetailsPlugin;
import org.dominokit.domino.ui.datatable.plugins.SelectionPlugin;
import org.dominokit.domino.ui.datatable.plugins.SortPlugin;
import org.dominokit.domino.ui.datatable.store.LocalListDataStore;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.infoboxes.InfoBox;
import org.dominokit.domino.ui.modals.ModalDialog;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.ColorScheme;
import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.api.ErrorCallback;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;
import org.jboss.gwt.elemento.core.IsElement;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Templated(value = "../template.html")
@Page(path = "admin/UserPage")
public class UserPage implements IsElement {
    @Inject
    private Caller<UserRest> testRestCaller;

    @Inject
    @DataField
    HTMLDivElement content;

    @PostConstruct
    private void preparePage() {
        RemoteCallback<List<User>> callback = users -> buildTable(users);
        ErrorCallback<String> errorCalback = (s, throwable) -> {
            Notification.createDanger("Rest comunication fail").show();
            return true;
        };
        testRestCaller.call(callback, errorCalback).getUsers("");
    }

    @Override
    public HTMLElement asElement() {
        return content;
    }

    private void buildTable(List<User> users) {
        LocalListDataStore<User> localListDataStore = new LocalListDataStore<>();
        TableConfig<User> tableConfig = new TableConfig<>();
        tableConfig.addPlugin(new RecordDetailsPlugin<>(cell -> new UserDetails(cell).asElement()));
        tableConfig.addPlugin(new SelectionPlugin<>(ColorScheme.BLUE));
        tableConfig.addPlugin(new SortPlugin<>());
        tableConfig.setMultiSelect(false);
        tableConfig
                .addColumn(ColumnConfig.<User>create("id", "#")
                        .textAlign("right")
                        .asHeader()
                        .setCellRenderer(cell -> new Text(cell.getTableRow().getRecord().getId().toString()))
                )

                .addColumn(ColumnConfig.<User>create("name", "name")
                        .setCellRenderer(cell -> new Text(cell.getTableRow().getRecord().getName())))

                .addColumn(ColumnConfig.<User>create("username", "User name")
                        .setCellRenderer(cell -> new Text(cell.getTableRow().getRecord().getUsername())));

        localListDataStore.setData(users);
        DataTable<User> table = new DataTable<>(tableConfig, localListDataStore);
        content.appendChild(Column.span12().appendChild(table).asElement());
        table.addSelectionListener((selectedTableRows, selectedRecords) ->
                displaySelectedUser(selectedRecords)
        );
        table.load();
    }

    private void displaySelectedUser(List<User> selectedRecords) {
        User user = selectedRecords.get(0);
        Notification.create("User selected: " + user.getId() + " " + user.getUsername() + " " + user.getName())
                .setPosition(Notification.BOTTOM_RIGHT)
                .show();


        ModalDialog modal = ModalDialog.create("Modal title");
        modal.appendChild(new Text("SAMPLE_CONTENT"));
        Button closeButton = Button.create("CLOSE").linkify();
        Button saveButton = Button.create("SAVE CHANGES").linkify();

        EventListener closeModalListener = evt -> modal.close();

        closeButton.addClickListener(closeModalListener);
        saveButton.addClickListener( closeModalListener);
        modal.appendFooterChild(saveButton);
        modal.appendFooterChild(closeButton);

        modal.open();
    }
}
